# OnTheMoney Test

Location-based services

## Installation

1. Clone this repo: click on the Clone button on the top right hand side of the page, select SSH, copy the link, paste it into your terminal and press the enter button. 
2. Type *__cd onthemoney/symfony-docker/__* into your terminal, where the repo is cloned
3. Type *__docker-compose up --build -d__* into your terminal and wait for the container to build successfully
4. Type *__cd symfony__* into your terminal
5. type *__composer install__* in your terminal and wait for all the required packages to install. ***Note:*** If you run into problem such as “PHP Fatal error: Allowed memory size exhausted”, instead, type: *__php -d memory_limit=-1 /usr/local/bin/composer install__*
6. While the composer is installing, Symfony will ask for missing parameters to be added. Please add the following:
```bash
database_host (127.0.0.1): mysql
database_port (null): 
database_name (symfony): 
database_user (root): 
database_password (null): password
mailer_transport (smtp): 
mailer_host (127.0.0.1): 
mailer_user (null): 
mailer_password (null): 
secret (ThisTokenIsNotSoSecretChangeIt): somesecret
```

## Usage

### To return postcodes with partial string matches as JSON, open your browser and type: 
* <http://localhost:8001/api/v1/postcode/NW10%204ER>
* <http://localhost:8001/api/v1/postcode/NW10>
* <http://localhost:8001/api/v1/postcode/NW1>
* <http://localhost:8001/api/v1/postcode/N1>

### To Get the closest postcodes based on Longitude and Latitude. 
##### This endpoint take the following: 
###### /coordinates/{longitude}/{latitude}/{distance}", defaults={"distance"=0.01} 
* <http://localhost:8001/api/v1/coordinates/-2.125766/57.137893>
* <http://localhost:8001/api/v1/coordinates/-2.125766/57.137893/5>

## Unit Test
To run unit tests, type: *__./vendor/bin/simple-phpunit__* in your terminal

## What is not completed
* JWT-Auth to authenticate the API endpoint to make sure it is secure
* More comprehensive unit testing to ensure data integrity but this is always good once the JSON response format has been decided by the backend and frontend dev team.
* Creation of Symfony console command to download the files and store them into the DB. I’d like to discuss this further during our chat. Currently, the database table only has three columns, which are Postcode, Longitude and Latitude to help creating the API endpoints
* Caching to make sure the load on the Database is reduced

