<?php

namespace Tests\AppBundle\Controller;

use GuzzleHttp\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


class PostcodeControllerTestControllerTest extends WebTestCase
{
    const BASE_URL = 'http://localhost:8001/api/v1/';

    public function testPostcode()
    {
        $client = new Client(['base_uri' => self::BASE_URL]);
        $response = $client->request('GET', 'postcode/NW100aa');
        $result = json_decode($response->getBody()->getContents(), true);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals(1, $result['count']);
        $this->assertArrayHasKey('count', $result);
        $this->assertArrayHasKey('result', $result);
        $this->assertEquals('NW100AA', $result['result'][0]);

    }

    public function testCoordinate()
    {
        $client = new Client(['base_uri' => self::BASE_URL]);
        $response = $client->request('GET', 'coordinates/-2.125766/57.137893');
        $result = json_decode($response->getBody()->getContents(), true);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals(2, $result['count']);
        $this->assertArrayHasKey('count', $result);
        $this->assertArrayHasKey('result', $result);
        $this->assertEquals('AB1 6SH', $result['result'][0]['postcode']);
        $this->assertEquals('AB106SH', $result['result'][1]['postcode']);

    }
}
