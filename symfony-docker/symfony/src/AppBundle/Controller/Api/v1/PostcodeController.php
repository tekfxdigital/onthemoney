<?php

namespace AppBundle\Controller\Api\v1;

use AppBundle\Entity\Postcode;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/v1")
 */
class PostcodeController extends Controller
{
    /**
     * Return postcodes with partial string matches as JSON
     *
     * @Route("/postcode/{postcode}" )
     * @param Request $request
     * @param $postcode
     * @return JsonResponse
     */
    public function postcodeAction(Request $request, $postcode)
    {
        $postcodeSearchResults = $this->getDoctrine()->getRepository(Postcode::class)->search($postcode);

        foreach($postcodeSearchResults as $postcodeSearchResult) {
            $value[] = $postcodeSearchResult['postcode'];
        }

        $data = [
            'count' => count($postcodeSearchResults),
            'result' => $value
        ];
        return new JsonResponse($data);
    }


    /**
     * Get the closest postcodes based on Longitude and Latitude
     *
     * @Route("/coordinates/{longitude}/{latitude}/{distance}", defaults={"distance"=0.01})
     * @param Request $request
     * @param $longitude
     * @param $latitude
     * @param $distance
     * @return JsonResponse
     */
    public function coordinatesAction(Request $request, $longitude, $latitude, $distance)
    {
        $coordinateResults = $this->getDoctrine()->getRepository(Postcode::class)->coordinates($longitude, $latitude, $distance);
        $data = [
            'count' => count($coordinateResults),
            'result' => $coordinateResults
        ];
        return new JsonResponse($data);
    }
}
